import java.util.Scanner;
import java.util.Random;

public class GuessNumber {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Random ac = new Random();
		for (int j = 1; j>0 ; j++) {
			System.out.println("请输入1-100之间的数字：");
		int guess = ac.nextInt(100) + 1;		
		for (int i = 1; i <= 100; i++) {
			int a = sc.nextInt();
			if (a == guess) {
				System.out.println("恭喜您  答对了");
				break;
			} else if (a > guess) {
				System.out.println("您猜的数字大了");
			} else {
				System.out.println("您猜的数字小了");
			}
			System.out.println("您还有" + (7 - i) + "次机会");
			if (i == 7) {
				System.out.println("很遗憾  你没机会了  正确的数字为" + guess);
				break;
			}

		}
		System.out.println("--------------------------------------");
		System.out.println("游戏结束，是否继续(是：1-否：0)");
		int x = sc.nextInt();
		if (x==0) {
			break;
		}
		}
		
	}
}
